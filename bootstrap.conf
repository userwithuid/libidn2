# Copyright (C) 2016 Tim Rühsen
# Copyright (C) 2016-2017 Simon Josefsson

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# bootstrapping docs
printf "gdoc_MANS =\ngdoc_TEXINFOS =\n" > doc/Makefile.gdoc

# gnulib library name
source_base=gl
gnulib_name=libgnu
use_libtool=1
SKIP_PO=1
checkout_only_file=

gnulib_modules="
error
gendocs
getline
gettext-h
gnupload
lib-symbol-versions
lib-symbol-visibility
maintainer-makefile
manywarnings
progname
stdio
strchrnul
strverscmp
update-copyright
valgrind-tests
version-etc
"

unistring_modules="
localcharset
unictype/joiningtype-of
libunistring
unistr/u32-cpy-alloc
uniconv/u8-strconv-to-locale
unistr/u32-strlen
uniconv/u8-strconv-from-locale
unictype/bidiclass-of
unictype/category-M
unictype/category-test
unictype/scripts
uninorm/base
uninorm/nfc
uninorm/u32-normalize
unistr/u32-to-u8
unistr/u8-to-u32
"

# Build prerequisites
buildreq="\
autoconf   2.62
automake   1.11.1
gettext    0.17
git        1.4.4
perl       5.5
"

bootstrap_post_import_hook ()
{
  # we re-use malloc-posix from the original gnulib
  for i in ${unistring_modules}; do
    sed -i 's/malloc-posix//g' ${GNULIB_SRCDIR}/modules/$i
  done
  ${GNULIB_SRCDIR}/gnulib-tool --without-tests --libtool --macro-prefix=unistring --lgpl=3orGPLv2 --dir=. --local-dir=unistring/override --lib=libunistring --source-base=unistring --m4-base=unistring/m4 --doc-base=doc --aux-dir=build-aux --avoid=posix-malloc --import ${unistring_modules}
  git -C ${GNULIB_SRCDIR} reset --hard
  # Automake requires that ChangeLog exist.
  touch ChangeLog || return 1
}
